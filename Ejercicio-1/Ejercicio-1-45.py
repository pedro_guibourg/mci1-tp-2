#Ejercicio 45: palíndromo

def es_palindromo (frase_in): # la función recibe un string con la frase o palabra
  frase = list (frase_in) # inicializa la lista "frase" asignandole el string recibido 
  i = 0 # inicializa la variable entera "i" asignandole el valor de 0. Esta variable maneja el ciclo while
  while i < len(frase): # el ciclo while se repite la cantidad de veces como letras hay en la palabra o frase (incluyendo los espacios) 
    if frase[i] == ' ': # si el i_esimo elemento de la lista "frase" es un espacio se procede a eliminarlo
      del frase[i] # se elimina dicho i_esimo elemento. Ver nota al pie 
    i +=1 # se suma 1 al valor de la variable entera "i" para que el ciclo no se repita infinitamente

  rev_frase = frase [::-1] # se inicializa la lista "rev_frase" asignandole la lista "frase" pero en sentido contrario
  if rev_frase == frase: # se evalua si la palabra o frase es palíndromo
    resultado = True # en caso de ser palíndromo se inicializa la variable booleana "resultado" asignandole el valor True
  else: # en cualquier otro caso (único caso: no ser palíndromo)
    resultado = False # en caso de no ser palíndromo se inicializa la variable booleana "resultado" asignandole el valor False
  return (resultado) # se devuelve la variable booleana "resultado"

frase = str (input("Ingrese la frase o palabra para analizar: ")) # se solicita que se ingrese una palabra o frase 
resultado = es_palindromo (frase.upper()) # se llama a la función pasandole el string "frase" pero todo en mayúsculas (estandarizado) 
# y se inicializa la variable booleana "resultado" asignandole el valor devuelto por la función
print(resultado) # se imprime el resultado en pantalla

# nota al pie: el comando del para eliminar un elemento de una lista lo tomé de:
# https://es.stackoverflow.com/questions/364514/eliminar-elemento-vac%C3%ADo-de-una-lista
# pero la rutina del while para eliminar todos los espacios vacios la creé yo 