# (1) Ejercicio 1: suma lenta
 
def suma(num_1, num_2): # la función recibe dos variables enteras con los números a sumar y devuelve una variable entera con el resultado
  resultado = num_1
  for i in range(num_2): # el ciclo for se repite una cantidad num_2 de veces 
    resultado += 1 
  return (resultado)

num_1 = int (input("Ingrese el primer número a sumar: "))
num_2= int (input("Ingrese el segundo número a sumar: "))
resultado = suma(num_1, num_2) # se inicializa la variable resultado y se le asigna el valor entero retornado por la función suma
print(num_1, "+", num_2, "=", resultado)