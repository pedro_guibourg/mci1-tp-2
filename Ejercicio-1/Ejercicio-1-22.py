# (6) Ejercicio 22: vocales

def es_vocal (letra): #la función recibe una letra y devuleve True si es vocal y False si es consonante
  vocales = ('A','E','I','O','U') #tumpla con todas las vocales para comparar
  for i in range(6): #realiza el ciclo 6 veces, 5 para comparar con las vocales y la última para asignar que no es vocal
    if i==5: #si es el sexto ciclo es porque no fue vocal (True) por lo tanto es consonante (False)
      resultado = False #es consonante, se asigna False a "resultado"
    elif letra.upper() == vocales[i]: #si la letra, pasada a mayusculas, es igual a alguna vocal se asigna True a "resultado"
      resultado = True #se asigna True a "resultado"
      break #si fue vocal no es necesario seguir comparando, se termina el ciclo
  return (resultado) #devuelve el valor booleano de "resultado" True si es vocal, False si es consonante

letra = str (input("Ingrese una letra: ")) #solicita que se ingrese una letra, no importa si es mayuscula o minuscula y se asigna en "letra"
resultado = es_vocal(letra) #llama a la función pasandole el string con los valores ingresados 
#y se inicializa la variable booleana "resultado" asignandole el valor devuelto por la función
print(resultado) #imprime el resultado en pantalla