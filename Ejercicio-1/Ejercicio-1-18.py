# (3) Ejercicio 18: suceción de Fibonacci

def fibonacci (valor): # la función recibe una variable entera con el número maximo hasta el que debe llegar la suseción y devuleve la sucesión
  inicio = (1,1) # se inicializa una tupla con los dos primeros números de la sucesión
  sucesion = list (inicio) # se la tupla anterior a lista, ya que las tuplas son inmutables
  for i in range(1, valor): # el ciclo for se repite tantas veces como el número máximo de la sucesión cargado
    if (sucesion[i-1]+sucesion[i])>valor:
      break # si la sucesión ya alcanzó el valor máximo se términa con el ciclo
    else:
      nuevo_elemento = sucesion[i-1]+sucesion[i]
      sucesion.append(nuevo_elemento)
  return (sucesion)  

max = int (input("Ingrese el último valor de la sucesion de Fibonacci: ")) 
sucesion = fibonacci(max) # se inicializa la lista sucesion y se le asigna el la lista retornada por la función fibonacci
print(sucesion) 