#Ejercicio 31: numerales romanos

def romanos (num):
  if num<=1000:
    if num==1000:
      romanos = list ("M")
    else:
      centenas = num//100
      decenas = (num-100*centenas)//10
      unidades = (num-100*centenas-10*decenas)
      
      #centenas
      if centenas==5:
        romanos = list ("D")
      elif centenas<=3 and centenas>0:
        romanos = list ("C"*centenas)
      elif centenas==4:
        romanos = list ("CD")
      elif centenas==9:
        romanos = list ("CM")
      elif centenas==0:
        None
      else:
        romanos = list ("D"+"C"*(centenas-5))   
      
      #decenas
      if decenas==5:
        romanos.append("L")
      elif decenas<=3 and decenas>0:
        romanos.append("X"*decenas)
      elif decenas==4:
        romanos.append("XL")
      elif decenas==9:
        romanos.append("XC")
      elif decenas==0:
        None
      else:
        romanos.append("L"+"X"*(decenas-5))   

      #unidades
      if unidades==5:
        romanos.append("V")
      elif unidades<=3 and unidades>0:
        romanos.append("I"*unidades)
      elif unidades==4:
        romanos.append("IV")
      elif unidades==9:
        romanos.append("IX")
      elif unidades==0:
        None
      else:
        romanos.append("V"+"I"*(unidades-5))   
    return (romanos)
    
  else:
    print("Te dije que sea menor o igual a 1000!!!")
  

num = int (input("Ingrese un número menor o igual a 1000: "))
num_romanos = romanos (num)
print(num_romanos)