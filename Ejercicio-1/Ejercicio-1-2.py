# (2) Ejercicio 2: grados Fahrenheit a centigrados
 
def conversion(t_faren): # la función recibe una variable flotante con la temp. en Fahrenheit y devuelve la temp. en centigrados
  t_cent = (5/9)*(t_faren-32)
  return (t_cent)
 
t_cent = conversion(float (input("Ingrese la temperatura en Fahrenheit: "))) # se inicializa la variable t_cent y se le asigna el valor flotante retornado por la función conversión
print(t_cent)