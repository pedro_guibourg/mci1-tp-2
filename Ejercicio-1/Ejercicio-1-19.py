# (4) Ejercicio 19: números primos

def es_primo (num): # la función recibe la variable entera con el número a analizar y devuelve True si es primo y False si no lo es
  divisores = 0
  for i in range(1, (num+1)): # el ciclo se repite entre 1 y el número a analizar (incluido)
    valor = num%i # se calcula el resto de dividir el número a analizar entre el valor temporal de i
    
    if valor == 0: 
      divisores += 1 # si el resto da cero se suma uno a los divisores

    if divisores > 2: 
      resultado = False # si los divisores son más que 2 ya se sabe que el número no es primo 
      break # si se determina que no es primo se corta el ciclo
    else:
      resultado = True
  return (resultado)

num = int (input("Ingrese el número a analizar: ")) 
resultado = es_primo(num) # se inicializa la variable booleana resultado y se le asigna el valor retornado por la función es_primo
print(resultado)