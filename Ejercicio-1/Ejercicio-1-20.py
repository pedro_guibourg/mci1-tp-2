# (5) Ejercicio 20: restas sucesivas

def restas (num_1, num_2): # la función recibe la variables enteras con los números a dividir y devuelve el cociente y el resto
  cociente = 0
  dif = num_1
  while (dif>0): # el ciclo while se repite si la difrencia entre los elementos de la resta no es negativa (para eviar seguir restando una vez hallado el cociente)
    dif=dif-num_2
    if dif>=0:
      cociente += 1 # si la diferencia no es negativa se suma uno al cociente 
      resto = dif
  return (cociente, resto)

num_1 = int (input("Ingrese el primer número: "))
num_2 = int (input("Ingrese el segundo número: "))
resultado = restas(num_1,num_2) # se inicializa la tupla resultado y se le asigna el valor retornado por la función restas
print("El cociente vale:", resultado[0], " y el resto vale:", resultado[1])