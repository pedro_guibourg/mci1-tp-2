# (7) Ejercicio 25: triangulos

def tipo_triangulo (lados_in): #la función recibe un string con los lados y devuelve un string con el resultado del analisis
  temp = lados_in.split(',') #separa el string "lados_in" por comas y lo asigna a la lista "temp" 
  lado_1 = float (temp[0]) #asigna el valor del lado 1 como variable flotante a "lado_1"
  lado_2 = float (temp[1]) #asigna el valor del lado 2 como variable flotante a "lado_2"
  lado_3 = float (temp[2]) #asigna el valor del lado 3 como variable flotante a "lado_3"

  if lado_1 == lado_2 and lado_1 == lado_3: #si dos lados son iguales entre si y uno de esos lados es igual al restante se sabe que es equilátero 
    resultado = "El triángulo es equilátero" #se inicializa el string "resultado" asignandole "El triángulo es equilátero" 
  else: #si el triángulo no es equilátero puede ser isósceles o escaleno
    if lado_1 == lado_2 or lado_1 == lado_3 or lado_2 == lado_3: #si cualquiera dos de sus lados son iguales es isósceles (considerando que no es equilátero)
      resultado = "El triángulo es isósceles" #se inicializa el string "resultado" asignandole "El triángulo es isósceles"
    else: #si el triángulo no es equilátero ni isósceles debe ser escaleno
      resultado = "El triángulo es escaleno" #se inicializa el string "resultado" asignandole "El triángulo es escaleno"
  return (resultado) #devuelve el string "resultado"

lados = str (input("Ingrese los lados de los triangulos separados por comas: ")) #solicita que se ingresen los valores de los lados del triángulo
resultado = tipo_triangulo(lados) #llama a la función pasandole el string con los valores ingresados 
#y se inicializa el string "resultado" asignandole el valor devuelto por la función
print(resultado) #imprime el resultado en pantalla